libimage-metadata-jpeg-perl (0.159-2) UNRELEASED; urgency=medium

  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 22 Dec 2022 07:09:26 -0000

libimage-metadata-jpeg-perl (0.159-1) unstable; urgency=medium

  * Move package to Debian Perl team
  * Add Homepage
  * Add watch file
  * debian/source/format: 3.0 (quilt)
  * DEP5
  * d/rules: Short dh
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Add missing ${misc:Depends} to Depends for libimage-metadata-jpeg-perl.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 07 Mar 2022 21:25:31 +0100

libimage-metadata-jpeg-perl (0.153-1.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "Removal of obsolete debhelper compat 5 and 6 in bookworm":
    Bump to 7 in debian/{compat,control}.
    (Closes: #965648)

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 Dec 2021 16:52:19 +0100

libimage-metadata-jpeg-perl (0.153-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "missing required debian/rules targets build-arch and/or build-
    indep": add missing targets to debian/rules.
    (Closes: #999061)

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Dec 2021 01:27:53 +0100

libimage-metadata-jpeg-perl (0.153-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 16:26:20 +0100

libimage-metadata-jpeg-perl (0.153-1) unstable; urgency=low

  * New upstream release
  * Changed arch to "any" instead of "all" (Closes: #465982)

 -- Rene Weber <rene_debmaint@public.e-mail.elvenlord.com>  Mon, 24 Jan 2011 20:41:29 +0100

libimage-metadata-jpeg-perl (0.15-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove /usr/share/perl5 only if it exists (needed to avoid FTBFS
    after Perl 5.10 transition) (Closes: #479945)

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 27 May 2008 00:11:41 -0500

libimage-metadata-jpeg-perl (0.15-1) unstable; urgency=low

  * Initial release (Closes: #458456)

 -- Rene Weber <rene_debmaint@public.e-mail.elvenlord.com>  Fri, 11 Jan 2008 19:42:44 +0100
